//
//  ViewController.m
//  testyopencv3
//
//  Created by John Fred Davis on 5/7/16.
//  Copyright © 2016 Netskink Computing. All rights reserved.
//

#import "ViewController.h"
#import <opencv2/opencv.hpp>

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@property (strong, nonatomic) IBOutlet UIButton *button;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)actionStart:(id)sender {
}

@end
