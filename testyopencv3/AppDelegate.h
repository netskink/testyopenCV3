//
//  AppDelegate.h
//  testyopencv3
//
//  Created by John Fred Davis on 5/7/16.
//  Copyright © 2016 Netskink Computing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

